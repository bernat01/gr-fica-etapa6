#include "Light.h"
#include <stdio.h>


//Constructores
Light::Light(GLenum n_light, GLfloat* pos, GLfloat* diffuse) :
	id{ n_light }, pos{ pos }, diffuse{diffuse} {}

Light::Light(GLenum n_light, GLfloat* pos, GLfloat* diffuse, GLfloat* especular) :
	id{ n_light }, pos{ pos }, diffuse{ diffuse }, especular{ especular } {}


void Light::init()
{
	if (this->status)
		glEnable(this->id);
	if (this->diffuse)
		glLightfv(this->id, GL_DIFFUSE, this->diffuse);
	/*if (this->especular)
		glLightfv(this->id, GL_SPECULAR, this->especular);
	if (this->ambient)
		glLightfv(this->id, GL_AMBIENT, this->ambient);*/

	glLightfv(this->id, GL_POSITION, this->pos);
}

void Light::changeState()
{
	if (this->status) {
		glDisable(this->id);
		this->status = false;
	}
	else {
		glEnable(this->id);
		this->status = true;
	}
}

void Light::incrementPos(GLint coordenada, GLfloat factor){ 
	this->pos[coordenada] += factor; 
}

GLfloat* Light::getPos() {
	return this->pos;
}

void Light::update() { 
	glLightfv(this->id, GL_POSITION, this->pos);
}

void Light::display() {
	glPushMatrix();		
	glColor4f(4.0, 4.0, 0.0, 1.0);
	glTranslatef(this->pos[0], this->pos[1], this->pos[2]);
	glutSolidSphere(0.2, 15, 15);
	glPopMatrix();
}

long Light::getId() {
	return this->id;
}

GLboolean Light::geStatus() {
	return this->status;
}