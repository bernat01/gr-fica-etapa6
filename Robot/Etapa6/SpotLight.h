#pragma once
#include "Light.h"
#include "utilities.h"

class SpotLight : public Light {

private:
	GLfloat* DirVector;
	GLfloat angulo;
	GLdouble expAtenuacion;

public:
	SpotLight(GLenum n_light, GLfloat* pos, GLfloat* diffuse, GLfloat* DirVector, GLfloat angulo, GLdouble expAtenuacion);
	void SpotLight::init();
	void SpotLight::update();
};