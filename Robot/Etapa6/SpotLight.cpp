#pragma once
#include "utilities.h"
#include <stdio.h>

#include "Light.h"
#include "SpotLight.h"

//Foco
SpotLight::SpotLight(GLenum n_light, GLfloat* pos, GLfloat* diffuse, GLfloat* DirVector, GLfloat angulo, GLdouble expAtenuacion) :
	Light{ n_light, pos, diffuse, diffuse }, DirVector{ DirVector }, angulo{ angulo }, expAtenuacion{ expAtenuacion } {}

void SpotLight::init() 
{	
	Light::init();
	glLightfv(this->getId(), GL_SPOT_DIRECTION, this->DirVector);
	glLightf(this->getId(), GL_SPOT_CUTOFF, this->angulo); //angulo del cono (0 - 90 || 180 (360 grados))
	glLightf(this->getId(), GL_SPOT_EXPONENT, this->expAtenuacion);//exponente de atenuacion  (0 - 128)
}

void SpotLight::update() {
	glLightfv(this->getId(), GL_POSITION, this->getPos());
}