#include <math.h>
#include "utilities.h"

GLdouble degToRad(GLdouble degree){ return degree * (PI / 180); }

GLdouble radToDeg(GLdouble radian){ return radian * (180 / PI); }

Point toCartesian(SphericalPoint p)
{
	Point res;
	res.y = p.r * sin(degToRad(p.fi)) * cos(degToRad(p.theta));
	res.x = p.r * sin(degToRad(p.fi)) * sin(degToRad(p.theta));
	res.z = p.r * cos(degToRad(p.fi));
	return res;
}

SphericalPoint toSpherical(Point p)
{
	SphericalPoint sp;
	sp.r = sqrt(pow(p.x, 2) + pow(p.y, 2) + pow(p.z, 2));

	sp.theta = radToDeg(acos(p.z/sp.r));
	
	sp.fi = radToDeg(atan(p.y / p.x));
	return sp;
}

void rotateVectorXZ(GLdouble angle, GLdouble* vector)
{
	
	vector[0] = vector[0] * cos(angle) - vector[2] * sin(angle);
	vector[1] = 0;
	vector[2] = vector[0] * sin(angle) + vector[2] * cos(angle);
	
}


GLdouble anguloXZ(GLdouble* vector)
{
	GLdouble exp = vector[0]  + vector[2];
	GLdouble div = sqrt(pow(vector[0], 2)+ pow(vector[2], 2));
	return radToDeg(exp / div);
}

void DibujaNormales(GLfloat x, GLfloat y, GLfloat z, GLfloat l) {
	glBegin(GL_LINES);

	glColor3f(0.0, 0.0, 1.0);//azul
	glVertex3f(x, y, z);
	glVertex3f(x + l, y, z);//Normal x	

	glColor3f(1.0, 0.0, 0.0);//Rojo
	glVertex3f(x, y, z);
	glVertex3f(x, y + l, z);//Normal y

	glColor3f(0.0, 1.0, 0.0);//Verde
	glVertex3f(x, y, z);
	glVertex3f(x, y, z + l);//Normal z	

	glEnd();
}