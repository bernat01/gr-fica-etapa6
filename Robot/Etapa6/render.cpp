#include "Init.h"
#include "Ambiente.h"
#include "Robot.h"
#include "SOIL\SOIL.h"

/* Texturas */
#define numTextures 2
#define PARQUET 0
#define LADRILLOS 1

int mode = 0;

//Entorno
GLfloat l_entorno = 10;

/* General */
Robot rob = Robot();

GLuint texturas[numTextures];
GLfloat s[numTextures];
GLfloat t[numTextures];

char *texturePaths[numTextures] = {
	"parquet.jpg",
	"ladrillos.jpg",
};

void initRender() 
{
	/* texturas  */
	for (int i = 0; i < numTextures; i++) {
		texturas[i] = SOIL_load_OGL_texture(texturePaths[i], SOIL_LOAD_RGB, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y || SOIL_FLAG_TEXTURE_REPEATS);
		glBindTexture(GL_TEXTURE_2D, texturas[i]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		/*glGetTexLevelParameterfv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, tam);*/
		//printf("%f W ", *tam);
		s[i] = l_entorno / 3;
		/*glGetTexLevelParameterfv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, tam);	*/
		//printf("%f H ", *tam);
		t[i] = l_entorno / 3;
	}
}
void Escena() 
{
	/* FOCOS */
	GLdouble radio = 2, tamCubo = 2, baseCono = 0.2, alturaCono = 0.2;
	glMatrixMode(GL_MODELVIEW);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	//Luces
	env_updateLights();
	//Camaras
	env_updateView();

	//Entorno
	glPushMatrix();
	//Suelo
	glColor3f(CL_BLANCO);
	glTranslatef(0.0f, rob.getYpie(), 0.0f);
	glBindTexture(GL_TEXTURE_2D, texturas[PARQUET]);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(-l_entorno, 0, -l_entorno);
	glTexCoord2f(s[PARQUET], 0.0);
	glVertex3f(l_entorno, 0, -l_entorno);
	glTexCoord2f(s[PARQUET], t[PARQUET]);
	glVertex3f(l_entorno, 0, l_entorno);
	glTexCoord2f(0.0, t[PARQUET]);
	glVertex3f(-l_entorno, 0, l_entorno);
	glEnd();
	//pared (-z)
	glBindTexture(GL_TEXTURE_2D, texturas[LADRILLOS]);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(-l_entorno, 0, l_entorno);
	glTexCoord2f(s[LADRILLOS], 0.0);
	glVertex3f(l_entorno, 0, l_entorno);
	glTexCoord2f(s[LADRILLOS], t[LADRILLOS]);
	glVertex3f(l_entorno, l_entorno, l_entorno);
	glTexCoord2f(0.0, t[LADRILLOS]);
	glVertex3f(-l_entorno, l_entorno, l_entorno);
	glEnd();
	//pared (x)
	glBegin(GL_QUADS);
	glTexCoord2f(s[LADRILLOS], t[LADRILLOS]);
	glVertex3f(-l_entorno, 0, -l_entorno);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(-l_entorno, l_entorno, -l_entorno);
	glTexCoord2f(s[LADRILLOS], 0.0);
	glVertex3f(-l_entorno, l_entorno, l_entorno);
	glTexCoord2f(0.0, t[LADRILLOS]);
	glVertex3f(-l_entorno, 0.0, l_entorno);
	glEnd();
	//pared (-x)
	glBegin(GL_QUADS);
	glTexCoord2f(s[LADRILLOS], t[LADRILLOS]);
	glVertex3f(l_entorno, 0, -l_entorno);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(l_entorno, l_entorno, -l_entorno);
	glTexCoord2f(s[LADRILLOS], 0.0);
	glVertex3f(l_entorno, l_entorno, l_entorno);
	glTexCoord2f(0.0, t[LADRILLOS]);
	glVertex3f(l_entorno, 0.0, l_entorno);
	glEnd();
	glPopMatrix();

	//Robot	
	glPushMatrix();
	rob.display();
	glPopMatrix();
	glutSwapBuffers();
}

void EspecialUp(int tecla, int x, int y) 
{
	switch (tecla) {
	case GLUT_KEY_UP:
	case GLUT_KEY_RIGHT:
	case GLUT_KEY_LEFT:
		rob.setMove(false);
		rob.resetMov();
		break;
	}
}

void robot_movement(int value) 
{
	if (rob.isMoving()) {//si s'ha aixecat la tecla UP deixam de mourens
		rob.robot_updateMove();
		env_cameraRelativeTo(rob.position);
		glutTimerFunc(100, robot_movement, 0);
	}
}

void robot_turnRight(int value) 
{
	if (rob.isMoving()) {//si s'ha aixecat la tecla UP deixam de mourens
						 //rob.robot_updateMove();
		env_cameraRelativeTo(rob.position);
		rob.girar(-10);
		glutTimerFunc(100, robot_turnRight, 0);
	}
}

void robot_turnLeft(int value) 
{
	if (rob.isMoving()) {//si s'ha aixecat la tecla UP deixam de mourens
						 //rob.robot_updateMove();
		env_cameraRelativeTo(rob.position);
		rob.girar(10);
		glutTimerFunc(100, robot_turnLeft, 0);
	}
}

void especialFunc(int key, int x, int y) 
{
		switch (key) {
		case GLUT_KEY_UP:
			if (!rob.isMoving()) {
				glutTimerFunc(100, robot_movement, 0);
				rob.setMove(true);
			}
			break;
		case GLUT_KEY_RIGHT:
			if (!rob.isMoving()) {
				glutTimerFunc(100, robot_turnRight, 0);
				rob.setMove(true);
			}
			break;
		case GLUT_KEY_LEFT:
			if (!rob.isMoving()) {
				glutTimerFunc(100, robot_turnLeft, 0);
				rob.setMove(true);
			}
			break;
		}
}