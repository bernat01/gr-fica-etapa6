#include "Ambiente.h"

/* CAMARA ACTUAL */
Point eye = { 0, 20, -20 }; //posici�n desde donde miramos la escena
Point upV = { 0.0, 1.0, 0.0 }; //vector normal Y
Point objective = { 0,0,0 }; //punto a donde queremos mirar
//valores de objective
GLint angleX = 0, angleY = 0, env_mode = 1;
int currentView = 3;//vista actual
int angle_inc = 10; //incremento de movimiento de camara

/* LUCES DE LA ESCENA */
int currentLight = 0; //current active light
GLfloat movLuz = 0.2f;//incremento en el movimiento de la luz
// valores iniciales para luces
GLfloat light_initialPos[4][4] = {
	{ 0.0f,0.0f,1.0f,0.0f },//global que ilumina en la dir. (0.0,0.0,-1.0)
	{ 0.0f,3.0f, -20.0f, 1.0f },//spot
	{ -2.0f,0.0f,-20.0f,1.0f },//spot
	{ 0.0f,0.0f,0.0f,1.0f } //spot
};
GLfloat colorLight[4] = { 1.0f,1.0f,1.0f,0.5f };
//GLfloat light_specular[4] = { 1.0f,1.0f,1.0f,1.0f };
//GLfloat light_diffuse[4] = { 1.0f,1.0f,1.0f,0.5f };
//GLfloat ambient[4] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat	luzDirVector[4][3] = {
	{ 0.0, -1.0, 0.0 },
	{ 0.0, 1.0, 0.0 },
	{ 1.0, 0.0, 0.0 },
	{ -1.0, 0.0, 0.0 }
};
//LUCES
Light lights[3] = {
	Light(GL_LIGHT0, light_initialPos[0], colorLight),
	//Light(GL_LIGHT1, light_initialPos[0], colorLight, colorLight),
	SpotLight(GL_LIGHT2, light_initialPos[1], colorLight, luzDirVector[0], 30.0, 10.0),//-y
	SpotLight(GL_LIGHT3, light_initialPos[2], colorLight, luzDirVector[3], 30.0, 10.0),//x
};


/*Luces*/
void env_setCurrentLight(GLint n) {
	currentLight = n;
}

void env_initLights()
{
	for (int i = 0; i < sizeof(lights) / sizeof(*lights); i++) {
		lights[i].init();
	}
	lights[0].changeState();
}

void env_updateLights()
{
	for (int i = 0; i < sizeof(lights) / sizeof(*lights); i++) {
		if (lights[i].geStatus()) {
			lights[i].update();
			lights[i].display();
		}
	}
}

void env_setPos(unsigned char key) {
	
	switch (key) {
	case 'a':
		lights[currentLight].incrementPos(2, -movLuz);
		break;
	case 's':
		lights[currentLight].incrementPos(1, movLuz);
		break;
	case 'd':
		lights[currentLight].incrementPos(2, movLuz);
		break;
	case 'z':
		lights[currentLight].incrementPos(0, -movLuz);
		break;
	case 'x':
		lights[currentLight].incrementPos(1, -movLuz);
		break;
	case 'c':
		lights[currentLight].incrementPos(0, movLuz);
		break;
	}
}

void env_changeState() {
	lights[currentLight].changeState();
}

//
///*Camara*/
// <summary>
// Cambia la vista de la camara entre cenital picado normal i contrapicado
// </summary>
//void changeView()
//{	
//	currentView++;
//	if (currentView > 4)
//		currentView = 0;
//	switch (currentView) {
//	case 0://cenital
//		upV = { 0, 1, 0 };
//		eye = { 90, 0, eye.r };
//		printf("cenital \n");
//		break;
//	case 1://picado
//		upV = { 0, 1, 0 };
//		eye = { 45, 0, eye.r };
//		printf("picado \n");
//		break;
//	case 2://normal
//		upV = { 0, 1, 0 };
//		eye = { 0, 0, eye.r };
//		printf("normal \n");
//		break;
//	case 3://contrapicado	
//		upV = { 0, 1, 0 };
//		eye = { -45, 0, eye.r };
//		printf("contrapicado \n");
//		break;
//	case 4://nadir	
//		upV = { 0, 1, 0 };
//		eye = { -90, 0, eye.r };
//		printf("nadir \n");
//		break;
//
//	default:
//		upV = { 0, 1, 0 };
//	}
//}
//*/
///// <summary>
///// Mueve la camara cuando se presiona una tecla del teclado.
///// </summary>
///// <param name="key"></param>
///// <param name="p"></param>
//int sphericalMove(int key, SphericalPoint* p) {
//	int angle_ing = 10;
//	switch (key)
//	{
//	case GLUT_KEY_UP:
//		p->fi += angle_ing;
//		break;
//
//	case GLUT_KEY_DOWN:
//		p->fi -= angle_ing;
//		break;
//
//	case GLUT_KEY_RIGHT:
//		//if (p->theta > -80) {
//		p->theta -= angle_ing;
//		//}
//		break;
//
//	case GLUT_KEY_LEFT:
//		//if (p->theta < 80) {
//		p->theta += angle_ing;
//		//}
//		break;
//
//	case GLUT_KEY_F1:
//		p->r += 1;
//		break;
//
//	case GLUT_KEY_F2:
//		p->r -= 1;
//		break;
//	default:
//		return 0;
//	}
//	return -1;
//}
/// <summary>
/// Incrementa el angulo hacia donde mira la camara
/// </summary>
/// <param name="key"></param>
//int moveObjective(int key) {
//	
//	switch (key)
//	{
//	case GLUT_KEY_UP:
//		if (angleX < 90) {
//			angleY += angle_inc;
//			objective.y = eye.r * sin(degToRad(angleY));
//		}
//		break;
//
//	case GLUT_KEY_DOWN:
//		if (angleX > -90) {
//			angleY -= angle_inc;
//			objective.y = eye.r * sin(degToRad(angleY));
//		}
//		break;
//
//	case GLUT_KEY_RIGHT:
//		if (angleX < 90) {
//			angleX += angle_inc;
//			objective.x = eye.r * sin(degToRad(angleX));
//		}
//		break;
//
//	case GLUT_KEY_LEFT:
//		if (angleX > -90) {
//			angleX -= angle_inc;
//			objective.x = eye.r * sin(degToRad(angleX));
//		}
//		break;
//
//	case GLUT_KEY_F1:
//		eye.r--;
//		break;
//
//	case GLUT_KEY_F2:
//		eye.r++;
//		break;
//	default:
//		return 0;
//	}
//	return -1;
//}
//int env_keyboardEvents(int key, int x, int y)
//{
//	if (key == GLUT_KEY_F4) {//swap eye <-> objective
//		env_mode = (env_mode == 0) ? 1 : 0;
//		return -1;
//	}
//
//	if (key == GLUT_KEY_F3) {
//		changeView();
//		return -1;
//	}
//
//	if (env_mode == 0) {//move camera
//		return sphericalMove(key, &eye);
//	}
//	else {//move objective
//		return moveObjective(key);
//	}
//}

/// <summary>
/// Configura la camera de manera que la seva posici� sigui relativa al 'center' 
/// </summary>
/// <param name="center"></param>
void env_cameraRelativeTo(Point center) 
{	
	objective = center;
	eye = center;
	eye.y += 20;
	eye.z += -20;
}

/*Actualiza la camara*/
void env_updateView() {
	glMatrixMode(GL_MODELVIEW);
	Point cartEye = eye;
	gluLookAt(cartEye.x, cartEye.y, cartEye.z, objective.x, objective.y, objective.z, upV.x, upV.y, upV.z);
}
