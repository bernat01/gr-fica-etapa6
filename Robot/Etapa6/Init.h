#pragma once
#include "utilities.h"

//Configuración de la ventana
#define W_WIDTH 800
#define W_HEIGHT 800
#define margenSuperior 100
#define margenDerecho 100
#define TITULO "Título de la ventana"

#define Near 2
#define Far 80

enum Perspective
{
	ORTHO, FRUSTUM, PERSPECTIVE
};

void InitGlut(int argc, char** argv);
void InitCallBacks(void(*escena)(), void(*idle)(), void(*reshape)(int w, int h));
void reshape(int win_width, int win_height, Perspective persp);