#include <glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#pragma once

class Light {

private:
	GLenum id;	
	bool status = false;
	GLfloat* pos;
	GLfloat* diffuse;
	GLfloat* especular;
	GLfloat* ambient;

public:
	Light(GLenum n_light, GLfloat* pos, GLfloat* diffuse);
	Light(GLenum n_light, GLfloat* pos, GLfloat* diffuse, GLfloat* especular);
	
	void Light::init();
	void Light::update();
	void Light::display();

	void Light::incrementPos(GLint coordenada, GLfloat factor);
	void Light::changeState();

	GLfloat* Light::getPos();
	long Light::getId();
	GLboolean Light::geStatus();
};


