#include "utilities.h"
#include "Light.h"
#include "SpotLight.h"

/* Robot */
class Robot {
	//Colores
	GLfloat clr_azul[4] = { CL_AZUL, 1 };
	GLfloat clr_verde[4] = { CL_VERDE, 1 };
	GLfloat clr_naranja[4] = { CL_NARANJA, 1 };

	void Robot::dibujaTronco();
	void Robot::dibujaBrazo(GLdouble* pos, GLboolean mov, GLboolean izq);
	void Robot::dibujaPierna(GLdouble* pos, GLboolean mov, GLboolean izq);
	void dibujaCabeza(GLdouble* pos, GLdouble radio, GLfloat* cEsfera);
public:	
	Point position = { 0.0, 0.0, 0.0};//pos del robot
	GLfloat posLuz[4] = {0.0, 0.0, 0.0, 1.0};

	/* Movimiento del Robot */
	GLboolean mov = false;
	GLfloat angHombroIzq = 0.0f, angCodoIzq = 0.0f;
	GLfloat angHombroDer = 0.0f, angCodoDer = 0.0f, dirHombro = 3.0f, dirCodo = dirHombro;
	GLfloat angPIzq = 0.0f, angPDer = 0.0f, dirPierna = dirHombro * 2;

	/* Variables principales */
	GLdouble r_hombro = 0.2f;
	/* Variables derivadas */
	GLfloat l_brazo = r_hombro * 4;
	GLdouble r_brazo = r_hombro*((GLdouble)2 / 3);
	GLdouble r_codo = r_hombro*((GLdouble)2 / 3);
	GLdouble bCono = r_hombro * 4, aCono = l_brazo * 2;
	GLfloat r_cabeza = r_hombro * 2.5;
	/* POSICIONES */
	GLboolean PIzq = true;
	GLboolean BIzq = true;

	GLdouble posCabeza[3] = { 0.0f, r_cabeza, 0.0f };
	GLdouble posBizq[3] = { -bCono, 0.0f, 0.0f };
	GLdouble posBder[3] = { bCono, 0.0f, 0.0f };
	GLdouble posPizq[3] = { -bCono / 2, -aCono*((GLfloat)2 / 3), 0.0f };
	GLdouble posPder[3] = { bCono / 2, -aCono*((GLfloat)2 / 3), 0.0f };
	GLfloat move = 0;
	GLfloat direccion[3] = { 0.0f, 0.0f, -1.0f };
	GLdouble angulo = 0;

	//SpotLight linterna = SpotLight(GL_LIGHT7, posLuz, clr_azul, direccion, 30.0, 10.0);

	Robot();
	void display();
	void robot_updateMove();
	bool isMoving();
	void resetMov();
	GLfloat getYpie();
	void setMove(GLboolean m);
    void Robot::girar(GLdouble a);
};
