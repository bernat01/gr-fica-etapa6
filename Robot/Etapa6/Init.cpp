#pragma once
#include "Init.h"

/// <summary>
/// Funci�n destinada a mantener el aspect/ratio del dibujo independientemente del
/// tama�o de la ventana
/// </summary>
/// <param name="width">Amplada de la ventana</param>
/// <param name="height">Altitud de la ventana</param>
/// <param name="persp">Tipo de perspectiva utilizada en la representaci�n</param>
void reshape(int width, int height, Perspective persp) {
	GLdouble aspectRatio;
	GLfloat xMin = -1.0f;
	GLfloat yMin = -1.0f;
	GLfloat xMax = 1.0f;
	GLfloat yMax = 1.0f;
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);

	if (width> height) //width greater than height
	{
		aspectRatio = (GLdouble)width / (GLdouble)height;

		xMax = aspectRatio;
		xMin = -aspectRatio;
	}
	else
	{
		aspectRatio = (GLfloat)height / (GLfloat)width;
		yMax = aspectRatio;
		yMin = -aspectRatio;
	}

	glLoadIdentity();

	switch (persp)
	{
	case ORTHO:
		glOrtho(xMin, xMax, yMin, yMax, Near, Far);
		break;
	case FRUSTUM:
		glFrustum(xMin, xMax, yMin, yMax, Near, Far);
		break;
	case PERSPECTIVE:
		gluPerspective(25, (GLdouble)width / (GLdouble)height, Near, Far);
		break;
	}

	glMatrixMode(GL_MODELVIEW);
}

void InitGlut(int argc, char** argv) {
	//Inicializamos biblioteca GLUT
	glutInit(&argc, argv);

	//Definimos los parametros de la ventana
	glutInitWindowPosition(margenDerecho, margenSuperior);
	glutInitWindowSize(W_WIDTH, W_HEIGHT);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	//RGBA = RGB + Alpha channel 
	//TODO: mirar modos de ventana
	glutCreateWindow(TITULO); //Creamos la ventana

	//asignamos negro al color de fondo de la pantalla
	glClearColor(CL_NEGRO, 1.0f);
}

void InitCallBacks(void(*escena)(), void(*idle)(), void(*reshape)(int w, int h)) {
	//Evento de cambio de tama�o de la ventana
	glutReshapeFunc(reshape);
	//Pasamos la escena asociada a la ventana
	glutDisplayFunc(escena);
	//Pasamos la escena que actualiza la escena
	glutIdleFunc(idle);
}