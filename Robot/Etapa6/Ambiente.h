#pragma once
#include "utilities.h"
#include "Light.h"
#include "SpotLight.h"


/*Luces*/
void env_initLights();
void env_updateLights();
void env_setCurrentLight(GLint n);
void env_setPos(unsigned char key);
void env_changeState();

/*Camara*/
//int env_keyboardEvents(int key, int x, int y);
void env_updateView();
void env_cameraRelativeTo(Point center);


