#include "Init.h"
#include "Ambiente.h"
#include "Robot.h"
#include "render.h"



/* SOMBREADO */
GLenum sombreado = GL_FLAT;
/** TRANSFORMACIONES ESCENA**/


void Idle()
{   //Repintamos escena
	glutPostRedisplay();
}
void Remodelar(int w, int h)
{
	reshape(w, h, PERSPECTIVE);
}

void Teclado(unsigned char tecla, int x, int y) {
	switch (tecla) {
		/* SELECCION DE LUZ */
	case '1':
	case '2':
	case '3':
	case '4':
		env_setCurrentLight(tecla - 49);//49 = codigo ACSI de '1' 
		break;
	case 'e':
		/* ENABLE/DISABLE THE CURRENT LIGHT */
		env_changeState();
		break;
	case 'a':
	case 's':
	case 'd':
  	case 'z':  
  	case 'x':  
  	case 'c':
		/* MODIFY THE POSITION OF THE CURRENT LIGHT */
		env_setPos(tecla);
		break;
	case ' ':
		/* SHADE */
		if (sombreado == GL_SMOOTH)	sombreado = GL_FLAT;
		else						sombreado = GL_SMOOTH;
		glShadeModel(sombreado);
		break;
	}
}



int main(int argc, char **argv) {
	GLfloat zPlane[] = { 0.0f, 0.0f, 1.0f, 0.0f };
	GLint w, h;
	GLfloat *tam = new GLfloat;
	
	//Configuramos Glut
	InitGlut(argc, argv);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
	glEnable(GL_BLEND); //habilitar blending
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //funcion de blending
	InitCallBacks(Escena, Idle, Remodelar);	glutKeyboardFunc(Teclado);
	glutSpecialFunc(especialFunc);
	glutSpecialUpFunc(EspecialUp);
	//glutTimerFunc(100, robot_movement, 0);
	glEnable(GL_TEXTURE_2D);
	/* Luces */
	env_initLights();
	/* Robot */
	initRender();
	
	
	
	
	/*glTexImage2D(GL_TEXTURE_2D, 0, 3, texturas[PARQUET].TexInfo->bmiHeader.biWidth, texturas[PARQUET].TexInfo->bmiHeader.biHeight, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, texturas[PARQUET].TexBits);*/	
	//texturas[PARQUET].s = texturas[PARQUET].t = l_entorno / sizeof(texturas[PARQUET].TexBits) / sizeof(GLubyte);//Solo para cuadradas
	
	//glDisable(GL_TEXTURE_2D);
	/*glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
	glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
	glTexGenfv(GL_S, GL_EYE_PLANE, zPlane);
	glTexGenfv(GL_T, GL_EYE_PLANE, zPlane);*/
	/*glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
	glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
	glTexGenfv(GL_S, GL_OBJECT_PLANE, zPlane);
	glTexGenfv(GL_T, GL_OBJECT_PLANE, zPlane);*/
	/*glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);*/
	//Activa la ventana, entra en bucle infinito y, comprueba la entrada de dispositivos
	glutMainLoop();
	return 0;
}