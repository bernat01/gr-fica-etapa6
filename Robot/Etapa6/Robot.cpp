#pragma once
#include "Robot.h"


GLint	def = 15;
GLUquadric* qobj;



/* Auxiliares */
void dibujaArticulacion(GLdouble radioEsfera, GLdouble radioCil, GLfloat l, GLfloat* cEsfera, GLfloat* cCil) 
{
	/* ESFERA */
	glColor4fv(cCil);
	glutSolidSphere(radioEsfera, def, def);
	//glTranslatef(0, -radioEsfera, 0);
	glPushMatrix();
	/* CILINDRO */
	glColor4fv(cEsfera);
	glRotatef(90, 1.0, 0.0, 0.0);
	gluCylinder(qobj, radioCil, radioCil, l, def, def);
	glPopMatrix();
}
void dibujaMano(GLdouble radio, GLfloat* cEsfera)
{

	glColor4fv(cEsfera);
	glutSolidSphere(radio, def, def);
}

/* Pintado del robot */
void Robot::dibujaCabeza(GLdouble* pos, GLdouble radio, GLfloat* cEsfera)
{
	glPushMatrix();
	glTranslatef(pos[0], pos[1], pos[2]);
	glColor4fv(cEsfera);
	glutSolidSphere(radio, def, def);
	glPopMatrix();
}
void Robot::dibujaTronco() 
{
	glPushMatrix();
	glColor4fv(clr_verde);
	glRotatef(90, 1.0, 0.0, 0.0);
	glutSolidCone(bCono, aCono, def, def);
	glPopMatrix();
	glPushMatrix();
	glColor4fv(clr_verde);
	glScalef(1.0, 0.3, 1.0f);
	glutSolidSphere(bCono, def, def);
	glPopMatrix();
}
void Robot::dibujaBrazo(GLdouble* pos, GLboolean mov, GLboolean izq)
{
	GLfloat angHombro = izq ? -angHombroIzq : -angHombroDer;
	GLfloat angCodo = izq ? -angCodoIzq : -angCodoDer;

	glPushMatrix();
		glTranslatef(pos[0], pos[1], pos[2]);
		if (mov) glRotatef(angHombro, 1.0, 0.0, 0.0);//mov brazo
		glPushMatrix();
			/* BRAZO SUP  */
			dibujaArticulacion(r_hombro, r_brazo, l_brazo, clr_azul, clr_azul);
			/* BRAZO INF */
			glTranslatef(0.0, -l_brazo, 0.0);//posicionar
			if (mov) glRotatef(angCodo, 1.0, 0.0, 0.0);//mov codo
			dibujaArticulacion(r_codo, r_brazo, l_brazo, clr_azul, clr_azul);
			/* MANO */
			glTranslatef(0.0, -l_brazo - r_hombro, 0.0);//posicionar
			dibujaMano(r_hombro, clr_azul);
		glPopMatrix();
	glPopMatrix();

}
void Robot::dibujaPierna(GLdouble* pos, GLboolean mov, GLboolean izq)
{
	GLfloat ang = izq ? angPIzq : angPDer;

	glPushMatrix();
		glTranslatef(pos[0], pos[1], pos[2]);
		if (mov && ang >= 0.0) glRotatef(ang, -1.0, 0.0, 0.0);
		glPushMatrix();
		/* PIERNA*/
		dibujaArticulacion(r_hombro, r_brazo, l_brazo, clr_azul, clr_azul);
		/* PIERNA INF */
		glTranslatef(0.0, -l_brazo, 0.0);//posicionar
		if (mov && ang >= 0.0) glRotatef(ang, 1.0, 0.0, 0.0);
		dibujaArticulacion(r_codo, r_brazo, l_brazo, clr_azul, clr_azul);
		/* PIE */
		glTranslatef(0.0, -l_brazo - r_hombro, 0.0);//posicionar
		dibujaMano(r_hombro, clr_azul);
		glPopMatrix();
	glPopMatrix();

}

/* Publicas del robot */
Robot::Robot() {
	qobj = gluNewQuadric();//para cilindro
	gluQuadricNormals(qobj, GLU_SMOOTH);
}

void Robot::display() 
{
	glTranslatef(this->position.x, this->position.y, this->position.z);
	glRotatef(angulo, 0.0f, 1.0f, 0.0f);
	dibujaTronco();

	dibujaCabeza(posCabeza, r_cabeza, clr_azul);

	dibujaBrazo(posBizq, mov, BIzq);
	dibujaBrazo(posBder, mov, !BIzq);

	dibujaPierna(posPizq, mov, PIzq);
	dibujaPierna(posPder, mov, !PIzq);
}

void Robot::robot_updateMove() {
	//if (angPIzq == 0 && angPDer == 0) {
		this->position.x += 0.5 *sin(degToRad(angulo));
		this->posLuz[0] += 0.5 *sin(degToRad(angulo));
		//this->position.y += this->direccion[1] * 1.1;
		this->position.z += 0.5*cos(degToRad(angulo));
		this->posLuz[2] += 0.5*cos(degToRad(angulo));
	//}
	

	if (angPIzq <= -50 || angPIzq >= 50) dirPierna = -dirPierna;
	angPIzq += dirPierna;
	angPDer += -dirPierna;

	if (angHombroIzq <= -25 || angHombroIzq >= 25) dirHombro = -dirHombro;
	angHombroIzq += -dirHombro;
	angHombroDer += dirHombro;

	if (!(angCodoIzq <= 0.0 && -dirHombro < 0))angCodoIzq += -dirHombro;
	if (!(angCodoDer <= 0.0 && dirHombro < 0))angCodoDer += dirHombro;

	
}
void Robot::resetMov() {
	angHombroIzq = 0.0f; angCodoIzq = 0.0f;
	angHombroDer = 0.0f; angCodoDer = 0.0f; dirHombro = 3.0f; dirCodo = dirHombro;
	angPIzq = 0.0f; angPDer = 0.0f; dirPierna = dirHombro * 2;
}

bool Robot::isMoving()
{
	return mov;
}

void Robot::girar(GLdouble a)
{
	angulo += a;
}
GLfloat Robot::getYpie() {
	GLfloat inicioPierna_Y = posPder[1];
	return inicioPierna_Y-(l_brazo * 2  + r_hombro*2);
}
void Robot::setMove(GLboolean m) {
	mov = m;
}