#pragma once
#include <glut.h>
#include <GL\GL.h>
#include <GL\GLU.h>

#include <stdio.h>
#include <Math.h>

#define DEBUG 1

#define PI 3.14159265

//Colores basicos
#define CL_ROJO 1.0, 0.0, 0.0 
#define CL_VERDE 0.0, 1.0, 0.0 
#define CL_AZUL 0.0, 0.0, 1.0
#define CL_AMARILLO 1.0, 1.0, 0.0 
#define CL_PURPURA 1.0, 0.0, 1.0 
#define CL_TURQUESA 0.0, 1.0, 1.0
#define CL_NARANJA  1.0, 0.5, 0.0
#define CL_BLANCO 1.0, 1.0, 1.0
#define CL_NEGRO  0.0, 0.0, 0.0
#define CL_GRIS   0.5, 0.5, 0.5
#define CL_MARRON 1.0, 0.0, 1.0

struct Point {
	GLdouble x;
	GLdouble y;
	GLdouble z;
};

struct SphericalPoint {
	GLdouble fi = 0;//angulo horizontal
	GLdouble theta = 90;//angulo vertical
	GLdouble r = 15;//distancia de la camera
};

GLdouble degToRad(GLdouble degree);
GLdouble radToDeg(GLdouble radian);
Point toCartesian(SphericalPoint p);
SphericalPoint toSpherical(Point p);
void rotateVectorXZ(GLdouble angle, GLdouble* vector);
GLdouble anguloXZ(GLdouble* vector);
void DibujaNormales(GLfloat x, GLfloat y, GLfloat z, GLfloat l);
